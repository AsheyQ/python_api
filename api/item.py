from models.models import ResponseModel
from models.requests import Client


class StoreItem:
    def __init__(self, url):
        self.url = url
        self.client = Client()

    POST_STORE_ITEM = '/item/'
    GET_ALL_ITEMS = '/items'

    def post_item(self, name_item, body: dict, headers):
        authorization = {"Authorization": f"JWT {headers}"}
        response = self.client.custom_request("POST", f"{self.url}{self.POST_STORE_ITEM}{name_item }",
                                              json=body, headers=authorization)
        return ResponseModel(status=response.status_code, response=response.json())

    def get_all_items(self, headers):
        authorization = {"Authorization": f"JWT {headers}"}
        response = self.client.custom_request("GET", f"{self.url}{self.GET_ALL_ITEMS}", headers=authorization)
        return ResponseModel(status=response.status_code, response=response.json())