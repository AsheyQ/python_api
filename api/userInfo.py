from jsonschema import validate

from models.models import ResponseModel
from models.requests import Client


class UserInfo:
    def __init__(self, url):
        self.url = url
        self.client = Client()

    POST_USER_INFO = '/user_info/'

    def get_user_info(self, user_id, body: dict, headers):
        authorization = {"Authorization": f"JWT {headers}"}
        response = self.client.custom_request("POST", f"{self.url}{self.POST_USER_INFO}{user_id}",
                                              json=body, headers=authorization)
        return ResponseModel(status=response.status_code, response=response.json())

    def add_user_info(self, user_id, body: dict, headers, schema: dict):
        authorization = {"Authorization": f"JWT {headers}"}
        response = self.client.custom_request("POST", f"{self.url}{self.POST_USER_INFO}{user_id}",
                                              json=body, headers=authorization)

        validate(instance=response.json(), schema=schema)
        return ResponseModel(status=response.status_code, response=response.json())