from models.models import ResponseModel
from models.requests import Client


class Auth:
    def __init__(self, url):
        self.url = url
        self.client = Client()

    POST_REGISTER_USER = '/auth'

    def auth_user(self, body: dict):
        response = self.client.custom_request("POST", f"{self.url}{self.POST_REGISTER_USER}", json=body)
        return ResponseModel(status=response.status_code, response=response.json())