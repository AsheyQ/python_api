from models.models import ResponseModel
from models.requests import Client


class Store:
    def __init__(self, url):
        self.url = url
        self.client = Client()

    _STORE = '/store/'

    def add_new_store(self, store, body, headers):
        authorization = {"Authorization": f"JWT {headers}"}
        response = self.client.custom_request("POST", f"{self.url}{self._STORE}{store}",
                                              json=body, headers=authorization)
        return ResponseModel(status=response.status_code, response=response.json())

    def get_store(self, store, headers):
        authorization = {"Authorization": f"JWT {headers}"}
        response = self.client.custom_request("GET", f"{self.url}{self._STORE}{store}",
                                              headers=authorization)
        return ResponseModel(status=response.status_code, response=response.json())