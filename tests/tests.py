import pytest


@pytest.mark.usefixtures('random_store', 'get_store', 'post_item', 'get_items')
class TestStore:
    def test_add_new_store(self, random_store):
        response_store = random_store
        assert response_store.status == 201

    def test_get_store(self, random_store, get_store):
        getStore = get_store
        assert getStore.status == 200

    def test_post_item(self, post_item):
        response_item = post_item
        assert response_item.status == 201

    def test_get_all_items(self, get_items):
        response_get_items = get_items
        assert response_get_items.status == 200


@pytest.mark.usefixtures('get_response', 'get_response_auth', 'get_user_info')
class TestUser:
    def test_registration(self, get_response):
        response = get_response
        assert response.status == 201
        assert response.response.get('message') == 'User created successfully.'
        assert response.response.get('uuid')

    def test_auth(self, get_response_auth):
        response_auth = get_response_auth
        assert response_auth.status == 200
        assert response_auth.response.get('access_token')

    def test_user_info(self, get_user_info):
        response_user_info = get_user_info
        assert response_user_info.status == 200
