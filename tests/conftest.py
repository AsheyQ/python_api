import pytest
from faker import Faker

from api.auth import Auth
from api.item import StoreItem
from api.register import Register
from api.store import Store
from api.userInfo import UserInfo
from models.models import DataFactory
from schemas.registration import valid_schema, valid_user_schema

"---------------------------TESTDATA---------------------------"
fake = Faker()
URL = "https://stores-tests-api.herokuapp.com"

store_name = fake.name()
body = DataFactory.random()
body_store = DataFactory.random_product()
body_user_info = DataFactory.random_user_info()
response = Register(url=URL).register_user(body=body, schema=valid_schema)
response_auth = Auth(url=URL).auth_user(body=body)
token = response_auth.response.get('access_token')
testStore = Store(url=URL).add_new_store(store=store_name, body=body_store, headers=token)

uuid = testStore.response.get('uuid')
user_info = UserInfo(url=URL).add_user_info(user_id=uuid, body=body_user_info, headers=token,
                                            schema=valid_user_schema)
"---------------------------TESTDATA---------------------------"


@pytest.fixture(scope='class')
def random_store():
    return testStore


@pytest.fixture(scope='class')
def get_store():
    return Store(url=URL).get_store(store=store_name, headers=token)


@pytest.fixture(scope='class')
def get_response():
    return response


@pytest.fixture(scope='class')
def get_response_auth():
    return response_auth


@pytest.fixture(scope='class')
def get_user_info():
    return user_info


@pytest.fixture(scope='class')
def post_item():
    body_item = DataFactory.random_store_item(uuid)
    return StoreItem(url=URL).post_item(name_item=fake.name(), body=body_item,
                                        headers=token)


@pytest.fixture(scope='class')
def get_items():
    body_item = DataFactory.random_store_item(uuid)
    for i in range(5):
        StoreItem(url=URL).post_item(name_item=fake.name(), body=body_item, headers=token)
    return StoreItem(url=URL).get_all_items(headers=token)
